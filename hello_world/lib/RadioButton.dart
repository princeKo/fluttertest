import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  static const _title = "Flutter Code Sample";
  
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: _title,
      home: Scaffold(
        appBar: AppBar(
          title: const Text(_title)),
        body: Center(
          child: MyStatefulWidget(),
        ),
      ),
    );
  }
}

enum SingingCharacter { lafayette, jefferson}

class MyStatefulWidget extends StatefulWidget {
  MyStatefulWidget({Key key}) : super(key: key);

  @override
  _MyStatefulWidgetState createState() => _MyStatefulWidgetState();

}

class _MyStatefulWidgetState extends State<MyStatefulWidget> {
  SingingCharacter _character = SingingCharacter.lafayette;

  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        ListTile(
          title: const Text("Lafayette"),
          leading: Radio(
            value: SingingCharacter.lafayette, 
            groupValue: _character, 
            onChanged: (SingingCharacter value) {
              setState(() {
                _character = value;
              });

              print("you chosed " + value.toString());

              _showMyDialog();
            },
          ),
        ),
        ListTile(
          title: const Text("Thomas Jefferson"),
          leading: Radio(
            value: SingingCharacter.jefferson, 
            groupValue: _character, 
            onChanged: (SingingCharacter value) {
              setState(() {
                _character = value;
              });

              print("you chosed " + value.toString());

              _showMyDialog();
            },
          ),
        ),
      ],
    );
  }
  

  Future<void> _showMyDialog() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          // title: Text("AlertDialog Title"),
          content:SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text("This is a demo alert dialog."),
                Text("Please click OK to proceed.")
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text("Cancel"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            FlatButton(
              child: Text("OK"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}

