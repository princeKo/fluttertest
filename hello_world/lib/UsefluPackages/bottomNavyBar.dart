import 'package:bottom_navy_bar/bottom_navy_bar.dart';
import 'package:flutter/material.dart';
import './../charts/barchart.dart';
import './../TabBarDemo.dart';
// import './../eCharts/sample.dart';
import 'package:settings_ui/settings_ui.dart';
import './SettingsPage/screens/settings_screen.dart';
void main() => runApp(MyBottomNavyBar());

class MyBottomNavyBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}


class _MyHomePageState2 extends State<MyHomePage> {

  int _currentIndex = 0;
  PageController _pageController;

  var account1 = SettingsList(
        sections: [
          SettingsSection(
            title: 'Section',
            tiles: [
              SettingsTile(
                title: 'Language',
                subtitle: 'English',
                leading: Icon(Icons.language),
                onTap: () {},
              ),
              SettingsTile.switchTile(
                title: 'Use fingerprint',
                leading: Icon(Icons.fingerprint),
                switchValue: false,
                onToggle: (bool value) {},
              ),
            ],
          ),
        ],
      );
    var account = SettingsScreen();

  @override
  void initState() {
    super.initState();
    _pageController = PageController();
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(title: Text("Nav Bar")),
      body: SizedBox.expand(
        child: PageView(
          controller: _pageController,
          onPageChanged: (index) {
            setState(() => _currentIndex = index);
          },
          children: <Widget>[
            // Container(color: Colors.blueGrey,),
            Container(child: TabBarDemo()),
            Container(color: Colors.red[100],),
            // Container(color: Colors.green,),
            // Container(child: MyChartApp()),
            Container(child: BarChartSample()),
            // Container(color: Colors.blue,),
            Container(child: account),
          ],
        ),
      ),
      bottomNavigationBar: BottomNavyBar(
        selectedIndex: _currentIndex,
        onItemSelected: (index) {
          setState(() => _currentIndex = index);
          _pageController.jumpToPage(index);
        },
        items: <BottomNavyBarItem>[
          BottomNavyBarItem(
            title: Text('Services'),
            icon: Icon(Icons.home)
          ),
          BottomNavyBarItem(
            title: Text('Discover'),
            icon: Icon(Icons.loyalty)
          ),
          BottomNavyBarItem(
            title: Text('Charts'),
            icon: Icon(Icons.show_chart)
          ),
          BottomNavyBarItem(
            title: Text('Account'),
            icon: Icon(Icons.person)
          ),
        ],
      ),
    );
  }
}







class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  // _MyHomePageState createState() => _MyHomePageState();
  _MyHomePageState2 createState() => _MyHomePageState2();
}

// class _MyHomePageState extends State<MyHomePage> {
//   int currentIndex = 0;
//   int _counter = 0;

//   void _incrementCounter() {
//     setState(() {
//       _counter++;
//     });
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: Text(widget.title),
//       ),
//       body: Center(
//         child: Column(
//           mainAxisAlignment: MainAxisAlignment.center,
//           children: <Widget>[
//             Text('You have pushed the button this many times:'),
//             Text(
//               '$_counter',
//               style: Theme.of(context).textTheme.display1,
//             ),
//           ],
//         ),
//       ),
//       floatingActionButton: FloatingActionButton(
//         onPressed: _incrementCounter,
//         tooltip: 'Increment',
//         child: Icon(Icons.add),
//       ),
//       bottomNavigationBar: BottomNavyBar(
//         selectedIndex: currentIndex,
//         showElevation: true,
//         itemCornerRadius: 8,
//         curve: Curves.easeInBack,
//         onItemSelected: (index) => setState(() {
//           currentIndex = index;
//         }),
//         items: [
//           BottomNavyBarItem(
//             icon: Icon(Icons.apps),
//             title: Text('Home'),
//             activeColor: Colors.red,
//             textAlign: TextAlign.center,
//           ),
//           BottomNavyBarItem(
//             icon: Icon(Icons.people),
//             title: Text('Users'),
//             activeColor: Colors.purpleAccent,
//             textAlign: TextAlign.center,
//           ),
//           BottomNavyBarItem(
//             icon: Icon(Icons.message),
//             title: Text(
//               'Messages test for mes teset test test ',
//             ),
//             activeColor: Colors.pink,
//             textAlign: TextAlign.center,
//           ),
//           BottomNavyBarItem(
//             icon: Icon(Icons.settings),
//             title: Text('Settings'),
//             activeColor: Colors.blue,
//             textAlign: TextAlign.center,
//           ),
//         ],
//       ),
//     );
//   }
// }