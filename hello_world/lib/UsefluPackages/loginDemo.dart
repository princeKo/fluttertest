import 'package:flutter/material.dart';
// import 'package:flutter_login_demo/services/authentication.dart';

import 'dart:async';
// import 'package:firebase_auth/firebase_auth.dart';
import 'package:http/http.dart' as http;
import 'package:http/io_client.dart';
import 'dart:io';

import 'dart:convert';

// import 'package:dio/dio.dart';

// void getHttp() async {
//   try {
//     Response response = await Dio().get("http://www.google.com");
//     print(response);
//   } catch (e) {
//     print(e);
//   }
// }
Map<String, dynamic> parseJwt(String token) {
  final parts = token.split('.');
  if (parts.length != 3) {
    throw Exception('invalid token');
  }

  final payload = _decodeBase64(parts[1]);
  final payloadMap = json.decode(payload);
  if (payloadMap is! Map<String, dynamic>) {
    throw Exception('invalid payload');
  }

  return payloadMap;
}

String _decodeBase64(String str) {
  String output = str.replaceAll('-', '+').replaceAll('_', '/');

  switch (output.length % 4) {
    case 0:
      break;
    case 2:
      output += '==';
      break;
    case 3:
      output += '=';
      break;
    default:
      throw Exception('Illegal base64url string!"');
  }

  return utf8.decode(base64Url.decode(output));
}

abstract class BaseAuth {
  Future<String> signIn(String email, String password);

  Future<String> signUp(String email, String password);

  // Future<FirebaseUser> getCurrentUser();

  // Future<void> sendEmailVerification();

  // Future<void> signOut();

  // Future<bool> isEmailVerified();
}

class Auth implements BaseAuth {
  // final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;

  // Future<String> signIn(String email, String password) async {
  //   AuthResult result = await _firebaseAuth.signInWithEmailAndPassword(
  //       email: email, password: password);
  //   FirebaseUser user = result.user;
  //   return user.uid;
  // }
  Future<String> signIn(String email, String password) async {
    print("HEHEHHEHE");

    var encodeNew ='Basic ' + base64Encode(utf8.encode('$email:$password'));
    // final response = await http.post(
    //   "https://10.6.88.55:4000/api/login",
    //   headers: {"Authorization": encodeNew},
    // );
    // print(response);
    // return "ok";

      var url = "https://10.6.88.55:4000/api/login";
      HttpClient httpClient = new HttpClient()
      ..badCertificateCallback =
      ((X509Certificate cert, String host, int port) => true);

      HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));


      request.headers.set('Authorization', encodeNew);

      HttpClientResponse response = await request.close();
      String reply = await response.transform(utf8.decoder).join();

      final body = json.decode(reply);

      print(response);
      print(reply);
      var token = body["token"];
      print(token);
      var newToken;
      var userID;
      if (token != null) {
         newToken = parseJwt(token);
         userID = newToken["userID"];
      }
      print(newToken);
      return userID;
  }

  Future<String> signUp(String email, String password) async {
    // AuthResult result = await _firebaseAuth.createUserWithEmailAndPassword(
    //     email: email, password: password);
    // FirebaseUser user = result.user;
    // return user.uid;
    return "abc";
  }

  // Future<FirebaseUser> getCurrentUser() async {
  //   FirebaseUser user = await _firebaseAuth.currentUser();
  //   return user;
  // }

  // Future<void> signOut() async {
  //   return _firebaseAuth.signOut();
  // }

  // Future<void> sendEmailVerification() async {
  //   FirebaseUser user = await _firebaseAuth.currentUser();
  //   user.sendEmailVerification();
  // }

  // Future<bool> isEmailVerified() async {
  //   FirebaseUser user = await _firebaseAuth.currentUser();
  //   return user.isEmailVerified;
  // }
}

void main() {
  runApp(MaterialApp(home: new LoginSignupPage(),)  );
}

Future<void> loginCallback(context, data) async {
  print("Login success!!!!");

  Timer(Duration(seconds: 3), () {
    // Navigator.of(context).pop(LoginSignupPage());
    // Navigator.of(context).pop(LoginSignupPage());
    Navigator.pop(context, data);
  });

}
class LoginSignupPage extends StatefulWidget {
  // LoginSignupPage({this.auth, this.loginCallback});
  // LoginSignupPage({ this.loginCallback});

  final BaseAuth auth = new Auth();
  // final VoidCallback loginCallback;

  @override
  State<StatefulWidget> createState() => new _LoginSignupPageState();
}

class _LoginSignupPageState extends State<LoginSignupPage> {
  final _formKey = new GlobalKey<FormState>();

  String _email;
  String _password;
  String _errorMessage;

  bool _isLoginForm;
  bool _isLoading;
  bool _isLoginSuccess;

  // String _userName;

  // Check if form is valid before perform login or signup
  bool validateAndSave() {
    final form = _formKey.currentState;
    print(_formKey);
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  // Perform login or signup
  void validateAndSubmit() async {
    setState(() {
      _errorMessage = "";
      _isLoading = true;
    });
    if (validateAndSave()) {
      String userId = "";
      try {
        if (_isLoginForm) {
          userId = await widget.auth.signIn(_email, _password);
          print('Signed in: $userId');
        } else {
          userId = await widget.auth.signUp(_email, _password);
          //widget.auth.sendEmailVerification();
          //_showVerifyEmailSentDialog();
          print('Signed up user: $userId');
        }
        setState(() {
          _isLoading = false;
          _isLoginSuccess = true;
        });

        if (userId.length > 0 && userId != null && _isLoginForm) {
          // widget.loginCallback();
          loginCallback(context, userId);
        }
      } catch (e) {
        print('Error: $e');
        setState(() {
          _isLoading = false;
          _errorMessage = e.message;
          _formKey.currentState.reset();
        });
      }
    }
  }

  @override
  void initState() {
    _errorMessage = "";
    _isLoading = false;
    _isLoginForm = true;
    _isLoginSuccess = false;
    // _userName = "";
    
    super.initState();
  }

  void resetForm() {
    _formKey.currentState.reset();
    _errorMessage = "";
  }

  void toggleFormMode() {
    resetForm();
    setState(() {
      _isLoginForm = !_isLoginForm;
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: new AppBar(
          title: new Text('Flutter login demo'),
        ),
        body: Stack(
          children: <Widget>[
            _showForm(),
            _showCircularProgress(),
          ],
        ));
  }

  Widget _showCircularProgress() {
    if (_isLoading) {
      return Center(child: CircularProgressIndicator());
    }
    return Container(
      height: 0.0,
      width: 0.0,
    );
  }

//  void _showVerifyEmailSentDialog() {
//    showDialog(
//      context: context,
//      builder: (BuildContext context) {
//        // return object of type Dialog
//        return AlertDialog(
//          title: new Text("Verify your account"),
//          content:
//              new Text("Link to verify account has been sent to your email"),
//          actions: <Widget>[
//            new FlatButton(
//              child: new Text("Dismiss"),
//              onPressed: () {
//                toggleFormMode();
//                Navigator.of(context).pop();
//              },
//            ),
//          ],
//        );
//      },
//    );
//  }

  Widget _showForm() {
    return new Container(
        padding: EdgeInsets.all(16.0),
        child: new Form(
          key: _formKey,
          child: new ListView(
            shrinkWrap: true,
            children:
            _isLoginSuccess ?
              <Widget>[
                showLogo(),
                showLoginSuccess(),
              ]
            :
              <Widget>[
                showLogo(),
                showEmailInput(),
                showPasswordInput(),
                showPrimaryButton(),
                showSecondaryButton(),
                showErrorMessage(),
              ],
          ),
        ));
  }

  Widget showErrorMessage() {
    if (_errorMessage.length > 0 && _errorMessage != null) {
      return new Text(
        _errorMessage,
        style: TextStyle(
            fontSize: 13.0,
            color: Colors.red,
            height: 1.0,
            fontWeight: FontWeight.w300),
      );
    } else {
      return new Container(
        height: 0.0,
      );
    }
  }

  Widget showLogo() {
    return new Hero(
      tag: 'hero',
      child: Padding(
        padding: EdgeInsets.fromLTRB(0.0, 70.0, 0.0, 0.0),
        child: CircleAvatar(
          backgroundColor: Colors.transparent,
          radius: 48.0,
          child: Image.asset('assets/flutter-icon.png'),
        ),
      ),
    );
  }

  Widget showLoginSuccess() {
    return new Container(
      child: Center(
        child: Column(children: <Widget>[
        // Container(padding: Padding(all: 100),)
          Icon(Icons.check_circle, color: Colors.green,),
          Text("Login Successfully"),
        ],),
        heightFactor: 8,
        // widthFactor: 20
        )

    );
  }

  Widget showEmailInput() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 100.0, 0.0, 0.0),
      child: new TextFormField(
        maxLines: 1,
        keyboardType: TextInputType.emailAddress,
        autofocus: false,
        decoration: new InputDecoration(
            hintText: 'Email',
            icon: new Icon(
              Icons.mail,
              color: Colors.grey,
            )),
        validator: (value) => value.isEmpty ? 'Email can\'t be empty' : null,
        onSaved: (value) => _email = value.trim(),
      ),
    );
  }

  Widget showPasswordInput() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
      child: new TextFormField(
        maxLines: 1,
        obscureText: true,
        autofocus: false,
        decoration: new InputDecoration(
            hintText: 'Password',
            icon: new Icon(
              Icons.lock,
              color: Colors.grey,
            )),
        validator: (value) => value.isEmpty ? 'Password can\'t be empty' : null,
        onSaved: (value) => _password = value.trim(),
      ),
    );
  }

  Widget showSecondaryButton() {
    return new FlatButton(
        child: new Text(
            _isLoginForm ? 'Create an account' : 'Have an account? Sign in',
            style: new TextStyle(fontSize: 18.0, fontWeight: FontWeight.w300)),
        onPressed: toggleFormMode);
  }

  Widget showPrimaryButton() {
    return new Padding(
        padding: EdgeInsets.fromLTRB(0.0, 45.0, 0.0, 0.0),
        child: SizedBox(
          height: 40.0,
          child: new RaisedButton(
            elevation: 5.0,
            shape: new RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(30.0)),
            color: Colors.blue,
            child: new Text(_isLoginForm ? 'Login' : 'Create account',
                style: new TextStyle(fontSize: 20.0, color: Colors.white)),
            onPressed: validateAndSubmit,
          ),
        ));
  }
}