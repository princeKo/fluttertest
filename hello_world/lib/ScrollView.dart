import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  static const _title = "Flutter Code Sample";

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      // title: _title,
      home: Scaffold(
        // appBar: AppBar(
        //   title: const Text(_title)),
        body: Center(
          child: CustomScrollView(
            slivers: <Widget>[
              const SliverAppBar(
                pinned: true,
                expandedHeight: 10.0,
                flexibleSpace: FlexibleSpaceBar(
                    // title: Text('Demo'),
                    ),
                backgroundColor: Colors.white,
              ),
              SliverGrid(
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  // maxCrossAxisExtent: 200.0,
                  crossAxisCount: 3,
                  mainAxisSpacing: 10.0,
                  crossAxisSpacing: 10.0,
                  // mainAxisSpacing: 0,
                  // crossAxisSpacing: 0,
                  childAspectRatio: 4.0,
                ),
                delegate: SliverChildBuilderDelegate(
                  (BuildContext context, int index) {
                    return Container(
                      height: 1000.00,
                      alignment: Alignment.center,
                      color: index % 9 == 0
                          ? Colors.teal[50]
                          : Colors.teal[100 * (index % 9)],
                      child: Text('Item $index'),
                    );
                  },
                  childCount: 11,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

// class ListView extends StatelessWidget {
//   final List<String> entries = <String>['A', 'B', 'C'];
//   final List<int> colorCodes = <int>[600, 500, 100];

//   ListView.separated(
//     padding: const EdgeInsets.all(8),
//     itemCount: entries.length,
//     itemBuilder: (BuildContext context, int index) {
//       return Container(
//         height: 50,
//         color: Colors.amber[colorCodes[index]],
//         child: Center(child: Text('Entry ${entries[index]}')),
//       );
//     },
//     separatorBuilder: (BuildContext context, int index) => const Divider(),
//   );

// }
