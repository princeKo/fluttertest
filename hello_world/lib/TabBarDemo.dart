import 'package:flutter/material.dart';
import 'package:transparent_image/transparent_image.dart';
// import 'webViewGoogle.dart';
import 'webView.dart';

void main() => runApp(TabBarDemo());


class TabBarDemo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    launchWebView();
    print("ABCDDDD");
    TestPrint.foo();
    return  MaterialApp(
      home: MyHome()
    );
    // return 
  }
}

// class Service {
//   final String title;
//   final String link;

//   Service(this.title, this.link);
// }


class QRcode extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      appBar: AppBar(title:Text("Scan to pay")),
      body: Center(
        child: Container(
          child: Image.asset('assets/qrCode.png'),
          width: 200,
          padding: const EdgeInsets.all(5),
        )
      )
    );
    // return qRcode;
  }
}

Route _createRoute(title, link) {
  return PageRouteBuilder(
    pageBuilder: title == "Payment"? (context, animation, secondaryAnimation) => QRcode() : (context, animation, secondaryAnimation) => WebViewExample(service: new Service(title, link)),
    transitionsBuilder: (context, animation, secondaryAnimation, child) {
      var begin = Offset(0.0, 1.0);
      var end = Offset.zero;
      var curve = Curves.ease;

      var tween = Tween(begin: begin, end: end).chain(CurveTween(curve: curve));

      return SlideTransition(
        position: animation.drive(tween),
        child: child,
      );
    },
  );
}

class MyHome extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var bocServeices = Scaffold(
                appBar: AppBar(
                  title: Center(
                    child: Text("Service List")
                  ) ,
                  backgroundColor: Colors.blueAccent[100],
                ),
                body: Center(
                  key: Key("This is a key"),
                  // child:Icon(Icons.directions_car),
                  child: GridView.count(
                    primary: false,
                    padding: const EdgeInsets.all(20),
                    crossAxisSpacing: 10,
                    mainAxisSpacing: 10,
                    crossAxisCount: 3,
                    // semanticChildCount: 5,
                    children: <Widget>[
                      Container(
                        height: 500.0,
                        padding: const EdgeInsets.all(8),
                        // child: const Text("Service 01"),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            IconButton(
                              icon: Icon(Icons.attach_money),
                              tooltip: 'Loan Application',
                              onPressed: () {
                                print("You clicked Loan Application");
                                Navigator.of(context).push(_createRoute("Loan", 'https://www.loan.com/'));
                                // setState(() {
                                //   _volume += 10;
                                // });
                              },
                            ),
                            Text("Loan")
                        ],), 
                        // color: Colors.teal[100],
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          // color: Colors.grey[200],
                          border: Border.all(color: Colors.grey[300]),
                          // boxShadow:
                          // shape: BoxShape.circle,
                          // boxShadow: 
                          // color: Colors.orange[200],
                        ),
                      ),
                      InkWell(
                        child: Container(
                          padding: const EdgeInsets.all(8),
                          child: Tab(icon: Icon(Icons.payment,),
                                      text: "Payment",
                                    ),
                          // color: Colors.teal[600],
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                            // borderRadius: BorderRadius.circular(10),
                            // color: Colors.white,
                            // boxShadow:
                            shape: BoxShape.circle,
                            color: Colors.orange[200],
                          ),
                        ),
                        onTap: (){
                          print("You clicked Payment");
                          // return WebViewExample();
                          // _pushService();
                          Navigator.of(context).push(_createRoute("Payment", 'https://www.google.com/'));
                        },
                      ),
                      InkWell(
                        child: Container(
                          padding: const EdgeInsets.all(8),
                          child: Column(
                            children: <Widget>[
                              Image(
                                image: AssetImage("images/youtube.png"),
                                width: 80.0
                              ),
                              Text("YouTube"),
                            ]
                          ),
                          // color: Colors.teal[200],
                          alignment: Alignment.center,
                        ),
                        onTap: (){
                          print("You clicked YouTube");
                          // return WebViewExample();
                          // _pushService();
                          Navigator.of(context).push(_createRoute("YouTube", 'https://www.youtube.com/'));
                        },
                      ),
                      InkWell(
                        child: Container(
                          padding: const EdgeInsets.all(8),
                          child: Column(
                            children: <Widget>[
                              Image(
                                image: AssetImage("images/google.png"),
                                width: 80.0
                              ),
                              Text("Google"),
                            ]
                          ),
                          // color: Colors.teal[200],
                          alignment: Alignment.center,
                        ),
                        onTap: (){
                          print("You clicked Google");
                          // return WebViewExample();
                          // _pushService();
                          Navigator.of(context).push(_createRoute('Google', 'https://www.google.com/'));
                        },
                      ),
                      InkWell(
                        child: Container(
                          padding: const EdgeInsets.all(8),
                          child: Column(
                            children: <Widget>[
                              Image(
                                image: AssetImage("images/twitter.png"),
                                width: 80.0
                              ),
                              Text("Twitter"),
                            ]
                          ),
                          // color: Colors.teal[200],
                          alignment: Alignment.center,
                        ),
                        onTap: (){
                          print("You clicked Twitter");
                          // return WebViewExample();
                          // _pushService();
                          Navigator.of(context).push(_createRoute("Twitter", 'https://www.twitter.com/'));
                        },
                      ),
                      InkWell(
                        child: Container(
                          padding: const EdgeInsets.all(8),
                          child: Column(
                            children: <Widget>[
                              Image(
                                image: AssetImage("images/instagram.png"),
                                width: 80.0
                              ),
                              Text("Instagram"),
                            ]
                          ),
                          // color: Colors.teal[200],
                          alignment: Alignment.center,
                        ),
                        onTap: (){
                          print("You clicked Instagram");
                          // return WebViewExample();
                          // _pushService();
                          Navigator.of(context).push(_createRoute("Instagram", 'https://www.instagram.com/'));
                        },
                      ),
                      InkWell(
                        child: Container(
                          padding: const EdgeInsets.all(8),
                          child: Column(
                            children: <Widget>[
                              Image(
                                image: AssetImage("images/facebook.png"),
                                width: 80.0
                              ),
                              Text("Facebook"),
                            ]
                          ),
                          // color: Colors.teal[200],
                          alignment: Alignment.center,
                        ),
                        onTap: (){
                          print("You clicked Facebook");
                          // return WebViewExample();
                          // _pushService();
                          Navigator.of(context).push(_createRoute("Facebook", 'https://www.facebook.com/'));
                        },
                      ),
                      InkWell(
                        child: Container(
                          padding: const EdgeInsets.all(8),
                          child: Column(
                            children: <Widget>[
                              Image(
                                image: AssetImage("images/whatsapp.png"),
                                width: 80.0
                              ),
                              Text("Whatsapp"),
                            ]
                          ),
                          // color: Colors.teal[200],
                          alignment: Alignment.center,
                        ),
                        onTap: (){
                          print("You clicked Whatsapp");
                          // return WebViewExample();
                          // _pushService();
                          Navigator.of(context).push(_createRoute("Whatsapp", 'https://www.whatsapp.com/'));
                        },
                      ),
                    ],
                  ),
                ),
                // backgroundColor: Colors.purple[50],
              );
      var scServeices = Scaffold(
              appBar: AppBar(
                title: Center(
                  child: Text("Service List")
                ) ,
                backgroundColor: Colors.blueAccent[100],
              ),
              body: Center(
                key: Key("This is a key"),
                // child:Icon(Icons.directions_car),
                child: GridView.count(
                  primary: false,
                  padding: const EdgeInsets.all(20),
                  crossAxisSpacing: 10,
                  mainAxisSpacing: 10,
                  crossAxisCount: 3,
                  // semanticChildCount: 5,
                  children: <Widget>[
                    Container(
                      height: 500.0,
                      padding: const EdgeInsets.all(8),
                      // child: const Text("Service 01"),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          IconButton(
                            icon: Icon(Icons.attach_money),
                            tooltip: 'Loan Application',
                            onPressed: () {
                              print("You clicked Loan Application");
                              Navigator.of(context).push(_createRoute("Loan", 'https://www.loan.com/'));
                              // setState(() {
                              //   _volume += 10;
                              // });
                            },
                          ),
                          Text("Loan")
                      ],), 
                      // color: Colors.teal[100],
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        // color: Colors.grey[200],
                        border: Border.all(color: Colors.grey[300]),
                        // boxShadow:
                        // shape: BoxShape.circle,
                        // boxShadow: 
                        // color: Colors.orange[200],
                      ),
                    ),
                    InkWell(
                      child: Container(
                        padding: const EdgeInsets.all(8),
                        child: Tab(icon: Icon(Icons.payment,),
                                    text: "Payment",
                                  ),
                        // color: Colors.teal[600],
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                          // borderRadius: BorderRadius.circular(10),
                          // color: Colors.white,
                          // boxShadow:
                          shape: BoxShape.circle,
                          color: Colors.orange[200],
                        ),
                      ),
                      onTap: (){
                        print("You clicked Payment");
                        // return WebViewExample();
                        // _pushService();
                        Navigator.of(context).push(_createRoute("Payment", 'https://www.google.com/'));
                      },
                    ),
                    InkWell(
                      child: Container(
                        padding: const EdgeInsets.all(8),
                        child: Column(
                          children: <Widget>[
                            Image(
                              image: AssetImage("images/youtube.png"),
                              width: 80.0
                            ),
                            Text("YouTube"),
                          ]
                        ),
                        // color: Colors.teal[200],
                        alignment: Alignment.center,
                      ),
                      onTap: (){
                        print("You clicked YouTube");
                        // return WebViewExample();
                        // _pushService();
                        Navigator.of(context).push(_createRoute("YouTube", 'https://www.youtube.com/'));
                      },
                    ),
                  ],
                ),
              ),
              // backgroundColor: Colors.purple[50],
            );
    // launchWebView();
    // print("ABCDDDD");
    // TestPrint.foo();
    return  MaterialApp(
      home: DefaultTabController(
        length:4 ,
        child: Scaffold(
          appBar: AppBar(
            bottom: TabBar(
              tabs: [
                Tab(
                  icon: Icon(Icons.account_balance),
                  text: "BOC",
                  // child: Text("Hey hey I am a child"), // child cannot be used in combination with text
                ),
                Tab(
                  icon: Icon(Icons.account_balance),
                  text: "SC",
                ),
                Tab(
                  icon: Icon(Icons.account_balance),
                  text: "HSBC",
                ),
                Tab(
                  icon: Icon(Icons.account_balance),
                  text:"Citi"
                ),
              ],
            ),
            title: Text('Services'),
          ),
          body: TabBarView(
            children: [
              // Icon(Icons.directions_car),
              bocServeices,
              // Icon(Icons.directions_transit),
              scServeices,
              // Center(
              //   child: FadeInImage.assetNetwork(
              //     placeholder: 'https://picsum.photos/250?image=5',
              //     image: 'https://picsum.photos/250?image=9',
              //   ),
              // ),
              // Icon(Icons.directions_bike),

              Stack(
                children: <Widget>[
                  Center(child: CircularProgressIndicator()),
                  Center(
                    child: FadeInImage.memoryNetwork(
                      placeholder: kTransparentImage,
                      image: 'https://github.com/flutter/plugins/raw/master/packages/video_player/video_player/doc/demo_ipod.gif?raw=true',
                    ),
                  ),
                ],
              ),

              // Image.network(
              //   'https://github.com/flutter/plugins/raw/master/packages/video_player/video_player/doc/demo_ipod.gif?raw=true',
              // ),
              // Icon(Icons.directions_walk),
              // Image.network(
              //   'https://picsum.photos/250?image=9',
              // )
              Stack(
                children: <Widget>[
                  Center(child: CircularProgressIndicator()),
                  Center(
                    child: FadeInImage.memoryNetwork(
                      placeholder: kTransparentImage,
                      image: 'https://picsum.photos/250?image=5',
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}

// Function launchWebView() {
//   print("Something like 1234");
//   return  ()=>{};
// }

void launchWebView() {
  print("Something like 1234");
}

class TestPrint {
  static void foo() {
    print("This is a static method");
  }
}


// var gridCells = GridView.count(
//   primary: false,
//   padding: const EdgeInsets.all(20),
//   crossAxisSpacing: 10,
//   mainAxisSpacing: 10,
//   crossAxisCount: 3,
//   // semanticChildCount: 5,
//   children: <Widget>[
//     Container(
//       height: 500.0,
//       padding: const EdgeInsets.all(8),
//       // child: const Text("Service 01"),
//       child: Column(
//         mainAxisSize: MainAxisSize.min,
//         children: <Widget>[
//           IconButton(
//             icon: Icon(Icons.attach_money),
//             tooltip: 'Loan Application',
//             onPressed: () {
//               print("You clicked Loan Application");
//               // setState(() {
//               //   _volume += 10;
//               // });
//             },
//           ),
//           Text("Loan")
//       ],), 
//       // color: Colors.teal[100],
//       alignment: Alignment.center,
//       decoration: BoxDecoration(
//         borderRadius: BorderRadius.circular(10),
//         // color: Colors.grey[200],
//         border: Border.all(color: Colors.grey[300]),
//         // boxShadow:
//         // shape: BoxShape.circle,
//         // boxShadow: 
//         // color: Colors.orange[200],
//       ),
//     ),
//     Container(
//       padding: const EdgeInsets.all(8),
//       child: const Text('Service 02'),
//       color: Colors.teal[200],
//       alignment: Alignment.center,
//     ),
//     Container(
//       padding: const EdgeInsets.all(8),
//       child: const Text('Service 03'),
//       color: Colors.teal[200],
//       alignment: Alignment.center,
//     ),
//     Container(
//       padding: const EdgeInsets.all(8),
//       child: const Text('Service 04'),
//       color: Colors.teal[200],
//       alignment: Alignment.center,
//     ),
//     Container(
//       padding: const EdgeInsets.all(8),
//       child: const Text('Service 05'),
//       color: Colors.teal[200],
//       alignment: Alignment.center,
//     ),
//     Container(
//       padding: const EdgeInsets.all(8),
//       child: const Text('Service 06'),
//       color: Colors.teal[200],
//       alignment: Alignment.center,
//     ),
//     InkWell(
//       child: Container(
//         padding: const EdgeInsets.all(8),
//         child: Tab(icon: Icon(Icons.payment,),
//                     text: "Payment",
//                   ),
//         // color: Colors.teal[600],
//         alignment: Alignment.center,
//         decoration: BoxDecoration(
//           // borderRadius: BorderRadius.circular(10),
//           // color: Colors.white,
//           // boxShadow:
//           shape: BoxShape.circle,
//           color: Colors.orange[200],
//         ),
//       ),
//       onTap: (){
//         print("You clicked Payment");
//         return WebViewExample();
//       },
//     ),
//   ],
// );